﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace HomeCinema.Services
{
    public interface IEncryptionService 
    { 
        string CreateSalt(); 
        string EncryptPassword(string password, string salt); 
    }
}
